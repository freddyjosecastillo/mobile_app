import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {ApiRestProvider} from'../../providers/api-rest/api-rest';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

 callsData: any[];
 loadingSpiner = 'loading';
 task : any;
  constructor(public navCtrl: NavController, private api: ApiRestProvider) {

  	this.task = setInterval(() => {
  		this.setCalls();
	}, 10000);
  	
  }

 	setCalls() {
		this.api.getArticles()
		.subscribe(data => {
			this.callsData = Array.of(data);
		    this.loadingSpiner = "noloading";
		     //console.log(this.callsData)
		});
	}

}
