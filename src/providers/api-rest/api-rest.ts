import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiRestProvider {
 configUrl = 'https://cp.hivepbx.com/api/api_app.php';

  constructor(public http: HttpClient) {
    //console.log('Hello ApiRestProvider Provider');
  }

  getArticles() {
  	return this.http.get(this.configUrl);
  }

}
